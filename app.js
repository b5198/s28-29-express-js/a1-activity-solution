const express = require("express");
const port = 4000;
const application = express();
application.listen(port, () => console.log (`Welcome to our Express API Server on port: ${port}`));
application.get ('/', (request, response) => {
	response.send('Greetings! Welcome to the to-do list of ROKKI GARAY');
})